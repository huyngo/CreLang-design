# CreLang

CreLang is a system-to-be that consists of various clients and decentralized
backup servers that facilitate documentation of conlang as well and spread them
among a community.

This document documents the objectives as well as analysis and design for the project.

## Table of contents

- [Background](background.md)
- [Objective](objective.md)
- [Use cases](uc.md)
- [Database](db.md)

-----

    Copyright (C)  2021  Ngô Ngọc Đức Huy.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
