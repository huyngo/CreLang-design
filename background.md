## Background

### ConWorkShop

**ConWorkShop** is a popular tool used among the conlang community. It offers:

- A builtin tool for documenting phonology, syntax, and vocabulary for a conlang
- A storage for the conlangs
- A community for sharing the conlangs
- All of the above services are given for free

However, there are several problems that refrains me from using it:

- It requires the user to choose some ambiguous 3-letter code, which is supposed
	to reflect the real life language code.  This limits the number of user
	to at most 17576 (not to mention that some user can have more than one language),
	which is problematic in and of itself.
	It also makes bad UX: Their users have to think of unique id and not the system. 
	Recently they increased the limit to 5-letters,
	which allows 12355928 users and mitigates the problem.
	Still, this is a bad decision and I would be against it.
- Its tools are limiting and rigid, which makes it hard to document some aspects
	of a conlang. For instance:
	- Phonology: It is very much against adding tones, and if your conlang
		has a lot of vowels, a few tones can be very troublesome to add
	- Syntax: They use their own meta-grammar (PhoMo) for syntax definition, refusing
		optional open standards like BNF or EBNF
- It is a SaaSS - Service as a Software Substitute.  Users who just want to create their
	own conlang using this tool have to create an account to use their service,
	instead of conveniently downloading that tool.
- It is not free software, therefore users cannot modify the tool to improve it
	or customize according to their need.

Its popularity, however, proves that if I can resolve these problems, I would probably
succeed.

### PolyGlot

**PolyGlot** is a JVM app that runs on GNU/Linux[^1], Windows, and MacOS.
It's also open-source and licensed under MIT License.

I haven't used this app much, so I haven't had much opinion on its usage.
However, by taking a look at this, it has a lot of features and cover a lot of usages.

What this app lacks are an online backup
(well, the users are free to use their own preferred service) and a mobile app. 

Its architecture however is very unclear to me, so I'd rather avoid working on it.

### Anki

Anki is a good example of what I want to achieve:

- Have unified, (open?) file format to store the data that can be easily backed up
- Have various clients on most popular platforms - Windows, MacOS, GNU/Linux, Android, iOS

[^1]: It actually only runs on only Debian-based distros, since the distribution file is a `.deb` file.
