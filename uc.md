# Use Cases

![Use case diagram, please see uml/uc.puml if you need screen reader](png/usecase.png)

## Create language

**Conlangers** can create as many languages as they want and the storage permits.
The flow of events goes as follow:

1. The **Conlanger** prompts the system to create a new language
2. The system receives the requests and open a form for the metadata of the language.
3. The **Conlanger** fills out the form and submit
4. The system accepts the form if it's valid and create a new entry in the database.
5. The system redirects the **Conlanger** to view the language.

## View language

**Conlangers** can view their languages, or others' languages if they're published.
They can view following details:

- Phonology and phonetics
	- Set of consonants, vowels, diphthongs
	- Pronunciation rules
	- Intonation, stress
- Grammar
	- Inflection
	- Determiner rules (articles, classifiers, ...)
	- Syntax:
		- Order in a sentence
		- Order in a phrase
- Morphology
	- How to derive new words
	- Affixes
	- Parts of speech
- Writing system
	- Orthography rules
	- Scripts
- Vocabulary
	- Word list and their meaning

## Update language

**Conlangers** can update each of the information listed in previous section (View language).

## Delete language

**Conlangers** can delete languages that they created. They should be warned before the deletion.

## Export document

**Conlangers** can export their languages as grammar book in following formats:

- PDF
- HTML
- LaTeX
- Markdown

They should also be able to export their dictionaries not only in above formats, but also as:

- Compressed DICT format
- Compressed StarDict format

## Export/Import backup

**Conlangers** can export their languages for backup as database file.
