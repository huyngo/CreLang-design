# Objective

## Overall

CreLang aims to be a system consisting:

- a file format for storing languages
- a client that can reads the file format and let the user edit it
	- the client SHOULD have a component that can export document from the app file to pdf, markdown, or html
- a storage for backup (optional, can use external services)
- a social network that allows people share their conlang (optional, maybe federated in the fediverse?)

## Supported platforms

CreLang will be implemented natively on GNU/Linux which I use, I also plan to
implement a JVM versions so that people using other desktop operating systems
can use it.  Additionall, it will be implemented natively on Android (which I
currently use at the time writing) and GNU/Linux for phone (which I plan to use
in the future). I will not support iOS, because I don't have a Macbook nor will
I have one, but anyone is welcome to implement one.

Additionally, I plan to make a self-hostable web app so that people using
unsupported operating systems can still use it.

## Legal

The software will be released under free licenses, particularly GPLv3+ (for the
native apps and JVM app) or AGPLv3+ (for the web app).
