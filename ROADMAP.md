# Roadmap

This starts out as a hobby project and I don't have time constraint for it.
Nonetheless, it's better to have a roadmap to see where I am and what to do
next.

## Phase 0: Planning

As I am writing this roadmap, we are at the end of this phase.

This does not mean that I will do no more planning, but rather that I will
start implementing rather than merely planning.

In this phase, I:

- drafted the use cases
- drafted the database design
- drafted implementation for grammar
- decided on the targeted platforms:
	- Desktop: GNU/Linux
	- Phone: Android, GNU/Linux
	- Other: Web, JVM

## Phase 1: Minimum Viable Product

In this MVP, it is expected that the following features be implemented:

- Phonology: The user can add common consonants and vowels.
  This should allow user to auto-generate spelling for new words and vice versa.
- Grammar: The user can add description. In this phase the inflection and
  syntax parsers are expected not to be finished yet.
- Vocabulary: User can add part of speeches and word classes.

The implementation will be on GNU/Linux, using GTK3. It should be able to be
ported to Windows and MacOS, though, as described in GTK docs.

## Phase 2: Elaboration

More features shall be implemented:

- Phonology: User can add tone and other suprasegmental features such as stress
  and nasal vowel.
- Grammar: User should be able to create inflection tables.
- Vocabulary: User can derivate new words using derivational rules defined in
  grammar

## Phase 3: Expand

In this phase, the application will be ported to different platforms.  Feedback
from users from previous phases will be used to enhance the app.
